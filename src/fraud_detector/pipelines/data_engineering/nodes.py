import json
from datetime import datetime
from typing import List

import pandas as pd
from sklearn.model_selection import train_test_split


def parse_raw_dataframe(df: pd.DataFrame) -> pd.DataFrame:
    df.dispositivo = df.dispositivo.apply(lambda x: dispositivo_to_dict(x))
    df.fecha = df.fecha.apply(lambda x: fecha_to_datetime(x))
    df.monto = df.monto.str.replace(',', '.').astype(float)
    df.cashback = df.cashback.str.replace(',', '.').astype(float)
    df.dcto = df.dcto.str.replace(',', '.').astype(float)
    df.interes_tc = df.interes_tc.astype(float)

    df = df.join(pd.json_normalize(df.dispositivo)).drop(columns=['dispositivo'])

    return df


def create_features(df: pd.DataFrame) -> pd.DataFrame:
    df['cantidad_txn_totales'] = df.groupby('ID_USER')['ID_USER'].transform('count')
    df['cantidad_txn_fraudes_totales'] = df[df.fraude == True].groupby('ID_USER')['ID_USER'].transform('count')
    df.cantidad_txn_fraudes_totales = df.cantidad_txn_fraudes_totales.fillna(0)

    return df


def remove_columns(df: pd.DataFrame, columns_to_remove: List[str]) -> pd.DataFrame:
    df = df.drop(columns_to_remove, axis=1)
    return df


def split_data(df: pd.DataFrame, test_size, random_state) -> dict:
    X = df.loc[:, df.columns != 'fraude']
    y = df.fraude
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)

    return dict(
        X_train=X_train,
        X_test=X_test,
        y_train=y_train,
        y_test=y_test
    )


def fecha_to_datetime(fecha: str) -> datetime:
    return datetime.strptime(fecha, '%d/%m/%Y')


def dispositivo_to_dict(dispositivo: str) -> dict:
    return json.loads(dispositivo.replace('\'', '"').replace(';', ','))
