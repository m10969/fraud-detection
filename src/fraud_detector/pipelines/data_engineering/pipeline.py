from kedro.pipeline import Pipeline, node

from .nodes import (
    parse_raw_dataframe,
    create_features,
    remove_columns,
    split_data
)


def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=parse_raw_dataframe,
                inputs=['mle_challenge_raw'],
                outputs='mle_challenge_primary',
                name='parse-data'
            ),
            node(
                func=create_features,
                inputs=['mle_challenge_primary'],
                outputs='enriched_data',
                name='create-features'
            ),
            node(
                func=remove_columns,
                inputs=['enriched_data', 'params:columns_to_remove'],
                outputs='depured_data',
                name='remove-columns'
            ),
            node(
                func=split_data,
                inputs=['depured_data', 'params:test_size', 'params:random_state'],
                outputs=dict(
                    X_train='X_train',
                    X_test='X_test',
                    y_train='y_train',
                    y_test='y_test'
                ),
                name='split-data'
            ),
        ]
    )
