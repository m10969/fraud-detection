import logging
import os
from typing import List

import joblib
import numpy as np
import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import (
    recall_score,
    f1_score,
    accuracy_score,
    precision_score,
    roc_auc_score,
    mean_squared_error,
    plot_confusion_matrix
)
from sklearn.naive_bayes import GaussianNB
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.preprocessing import OneHotEncoder, StandardScaler, FunctionTransformer
import matplotlib.pyplot as plt


MODEL_PATH = os.path.join(os.getcwd(), 'data/06_models')
MODEL_OUTPUT_PATH = os.path.join(os.getcwd(), 'data/07_model_output')


def create_transformer(categorical_columns: List[str], numerical_columns: List[str]) -> ColumnTransformer:
    return ColumnTransformer([
        ('one-hot-encoder', OneHotEncoder(), categorical_columns),
        ('scaler', StandardScaler(), numerical_columns)
    ])


def create_pipeline(transformer: ColumnTransformer, penalty: str, class_weight: str) -> Pipeline:
    return make_pipeline(
        transformer,
        FunctionTransformer(lambda x: x.todense(), accept_sparse=True),
        SelectFromModel(LogisticRegression(penalty=penalty)),
        LogisticRegression(class_weight=class_weight)
    )


def train_model(X_train: pd.DataFrame, y_train: pd.DataFrame, penalty: str, class_weight: str, categorical_columns: List[str], numerical_columns: List[str]) -> Pipeline:
    transformer = ColumnTransformer([
        ('one-hot-encoder', OneHotEncoder(), categorical_columns),
        ('scaler', StandardScaler(), numerical_columns)
    ])

    pipe = make_pipeline(
        transformer,
        FunctionTransformer(lambda x: x.todense(), accept_sparse=True),
        # SelectFromModel(LogisticRegression(penalty=penalty)),
        # LogisticRegression(class_weight=class_weight)
        GaussianNB()
    )
    pipe.fit(X_train, y_train)

    return pipe


def predict(pipe: Pipeline, X_test: pd.DataFrame) -> np.ndarray:
    y_test = pipe.predict(X_test)
    disp = plot_confusion_matrix(pipe, X_test, y_test, normalize='true', cmap=plt.cm.Blues)

    path = os.path.join(os.getcwd(), f'{MODEL_OUTPUT_PATH}/confusion_matrix.png')
    plt.savefig(path)

    return y_test


def save_model(pipe: Pipeline):
    joblib.dump(pipe[-1], f'{MODEL_PATH}/fraud_detector.pkl', compress=1)


def report_accuracy(y_pred: np.ndarray, y_test: pd.DataFrame) -> None:
    """Node for reporting the accuracy of the predictions performed by the
    previous node. Notice that this function has no outputs, except logging.
    """
    rmse = np.sqrt(mean_squared_error(y_test.astype(int), y_pred.astype(int)))

    log = logging.getLogger(__name__)
    msg = f'Recall: {recall_score(y_test, y_pred)}\n' \
          + f'F1: {f1_score(y_test, y_pred)}\n' \
          + f'Accuracy: {accuracy_score(y_test, y_pred)}\n' \
          + f'precision: {precision_score(y_test, y_pred)}\n'\
          + f'roc auc: {roc_auc_score(y_test, y_pred)}\n' + f'RMSE: {rmse}\n\n'

    path = os.path.join(os.getcwd(), f'{MODEL_OUTPUT_PATH}/metrics.txt')

    with open(path, 'w') as outfile:
        outfile.write(str(msg))

    log.info(msg)


def get_selected_feature_names(pipe: Pipeline,  X_test: pd.DataFrame) -> List[str]:
    model = SelectFromModel(pipe[-1], prefit=True)
    feature_idx = model.get_support()
    feature_name = pd.DataFrame(pipe[0].transform(X_test).todense()).columns[feature_idx]
    return list(feature_name)