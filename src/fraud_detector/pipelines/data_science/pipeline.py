from kedro.pipeline import Pipeline, node

from .nodes import (
    train_model,
    predict,
    report_accuracy,
    save_model,
)


def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=train_model,
                inputs=['X_train', 'y_train', 'params:penalty', 'params:class_weight', 'params:categorical_columns', 'params:numerical_columns'],
                outputs='trained_model',
                name='train',
            ),
            node(
                func=predict,
                inputs=['trained_model', 'X_test'],
                outputs='y_pred',
                name='predict',
            ),
            node(
                func=save_model,
                inputs=['trained_model'],
                outputs=None,
                name='save',
            ),
            node(
                func=report_accuracy,
                inputs=['y_pred', 'y_test'],
                outputs=None,
                name='report',
            ),

            # node(
            #     func=get_selected_feature_names,
            #     inputs=['trained_model', 'X_test'],
            #     outputs='feature_names',
            #     name='feature-names',
            # ),
        ]
    )
